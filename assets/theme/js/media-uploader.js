/* jshint onevar: false, multistr: true */
/* global _wpMediaViewsL10n, _wpGalleryWidgetAdminSettings */

(function($){
    var $id;
    var $thumb;
    var $title;
    var $link;

    $(function(){
        $( document.body ) .on( 'click', '.hero-widget-choose-images', function( event ) {
            event.preventDefault();

            var widget_form = $( this ).closest( 'form, .form' );

            $id     = widget_form.find( '.hero-widget-id' );
            $thumb	= widget_form.find( '.hero-widget-thumb' );
            $title	= widget_form.find( '.hero-widget-title' );
            $link	= widget_form.find( '.hero-widget-link' );

            var idString = $id.val();

            file_frame = wp.media.frames.file_frame = wp.media({
                frame:    'post',
                state:    'insert',
                multiple: false
            });

            /**
             * Setup an event handler for what to do when an image has been
             * selected.
             *
             * Since we're using the 'view' state when initializing
             * the file_frame, we need to make sure that the handler is attached
             * to the insert event.
             */
            file_frame.on( 'insert', function() {

                // Read the JSON data returned from the Media Uploader
                json = file_frame.state().get( 'selection' ).first().toJSON();

                // First, make sure that we have the URL of an image to display
                if ( 0 > $.trim( json.url.length ) ) {
                    return;
                }

                $id.val( json.id );
                $thumb.empty().html('<img src="'+json.url+'" style="max-width:100%;" />');
                if ( undefined == $title.val() || $title.val().length == 0 ) {
                    $title.val( json.title );
                }
                if ( undefined == $link.val() || $link.val().length == 0 ) {
                    $link.val( json.link );
                }

            });

            file_frame.open();
        });
    });
})(jQuery);
