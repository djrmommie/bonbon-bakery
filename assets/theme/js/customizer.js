/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {
	wp.customize('color_scheme',function( value ) {
		value.bind( function( to ) {
			var classes = $( 'body' ).attr( 'class' ).replace( /color-scheme-[a-zA-Z0-9_-]*/g, '' );
			$( 'body' ).attr( 'class', classes ).addClass( 'color-scheme-' + to );
		});
	});
} )( jQuery );
