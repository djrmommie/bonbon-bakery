// https://github.com/gruntjs/grunt-contrib-copy
module.exports = {
	css: {
		files: [
			{
				cwd: '<%= paths.tmp %>',
				expand: true,
				flatten: false,
				src: ['**/*.css','!style*.css', '!style*.map'],
				dest: '<%= paths.theme %>/css',
				filter: 'isFile'
			}
		]
	},
	theme: {
		files: [
			{
				cwd: '<%= paths.tmp %>',
				expand: true,
				flatten: false,
				src: ['style*.css', 'style*.map'],
				dest: '<%= paths.theme %>',
				filter: 'isFile'
			}
		]
	},
	js: {
		files: [
			{
				cwd: '<%= paths.authorAssets %>js/',
				expand: true,
				flatten: false,
				src: ['*','!theme.js'],
				dest: '<%= paths.theme %>js/',
				filter: 'isFile'
			}
		]
	},
    jsAssets: {
        files: [
            {
                cwd: '<%= paths.bower %>',
                expand: true,
                flatten: true,
                src: ['html5shiv/js/html5.min.js'],
                dest: '<%= paths.theme %>js/',
                filter: 'isFile'
            }
        ]
    },
	hybridcore: {
		files: [
			{
				cwd: '<%= paths.composer %>justintadlock/hybrid-core',
				expand: true,
				src: ['**/*'],
				dest: '<%= paths.hybridCore %>'
			}
		]
	},
	images: {
		files: [
			{
				cwd: '<%= paths.tmp %>images',
				expand: true,
				flatten: true,
				src: ['*', '!screenshot.png'],
				dest: '<%= paths.theme %>images',
				filter: 'isFile'
			}
		]
	},
	screenshot: {
		files: [
			{
				cwd: '<%= paths.tmp %>images',
				expand: true,
				flatten: true,
				src: ['screenshot.png'],
				dest: '<%= paths.theme %>',
				filter: 'isFile'
			}
		]
	},
	languages: {
		files: [
			{
				cwd: '<%= paths.assets %><%= paths.languages %>',
				expand: true,
				src: ['*.po'],
				dest: '<%= paths.theme%><%= paths.languages %>',
				filter: 'isFile'
			}
		]
	},
	release: {
		expand: true,
		cwd: '../',
		src: ['**', '!dist/**', '!readme.md', '!.gitignore', '!assets/**'],
		dest: 'build/'
	}
};
