// https://github.com/gruntjs/grunt-contrib-cssmin
module.exports = {
	options: {
		report: 'gzip',
        sourceMap: false
	},
	style: {
		expand: true,
		cwd: '<%= paths.tmp %>',
		src: [
			'**/*.css',
			'!*.min.css'
		],
		dest: '<%= paths.tmp %>',
		ext: '.min.css',
        sourcemap: '<%= paths.tmp %>/style.css.map'
	},
	vendor: {
		expand: true,
		cwd: '<%= paths.theme %>css/',
		src: [
            '**/*.css',
            '!*.min.css'
		],
		dest: '<%= paths.theme %>css/',
		ext: '.min.css'
	}
};
