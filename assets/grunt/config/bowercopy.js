// https://github.com/timmywil/grunt-bowercopy
module.exports = {
	options: {
		clean: true,
		ignore: ['jquery']
	},
	css: {
		options: {
			destPrefix: '<%= paths.bower %>'
		},
		files: {
			'font-awesome': 'font-awesome',
			'unsemantic': 'unsemantic/assets/sass'
		}
	},
	js: {
		options: {
			destPrefix: '<%= paths.bower %>'
		},
		files: {
			'html5shiv/js': 'html5shiv/dist/html5shiv.min.js'
		}
	}
};
