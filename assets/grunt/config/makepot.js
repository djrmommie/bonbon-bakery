// https://github.com/blazersix/grunt-wp-i18n
module.exports = {
	theme: {
		options: {
			cwd: '<%= paths.theme %>',
            exclude: ['assets/.*'],
			domainPath: '<%= paths.languages %>',
			potHeaders: {
				poedit: true
			},
			type: 'wp-theme'
		}
	}
};
