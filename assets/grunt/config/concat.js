// https://github.com/gruntjs/grunt-contrib-concat
module.exports = {
	themejs: {
		src: [
			'<%= paths.authorAssets %>js/theme.js'
		],
		dest: '<%= paths.theme %>js/theme.js'
	}
};
