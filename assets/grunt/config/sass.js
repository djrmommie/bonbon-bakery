// https://github.com/gruntjs/grunt-contrib-sass
module.exports = {
    options: {
        force: true,
        style: 'expanded',
        trace: true,
        lineNumbers: false,
        sourcemap: 'none'
    },
    theme: {
        options: {
            sourcemap: 'auto'
        },
        files: [
            {
                expand: true,
                cwd: '<%= paths.authorAssets %>scss/',
                src: 'style.scss',
                dest: '<%= paths.tmp %>',
                ext: '.css'
            }
        ]
    },
    css: {
        options: {
            force: true,
            style: 'expanded',
            trace: true,
            lineNumbers: false,
            sourcemap: 'none'
        },
        files: [
            {
                expand: true,
                cwd: '<%= paths.authorAssets %>scss/css/',
                src: ['**/*.scss', '!editor-style.scss'],
                dest: '<%= paths.tmp %>',
                ext: '.css'
            }
        ]
    },
    editorstyle: {
        options: {
            sourcemap: 'none',
            lineNumbers: false
        },
        files: [
            {
                expand: true,
                cwd: '<%= paths.authorAssets %>scss/',
                src: 'editor-style.scss',
                dest: '<%= paths.tmp %>',
                ext: '.css'
            }
        ]
    }
};
