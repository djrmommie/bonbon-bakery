<?php for ($x = 1; $x <= 5; $x++) : ?>

	<?php if ( is_active_sidebar( 'home-' . $x ) ) : // If the sidebar has widgets. ?>

		<aside <?php hybrid_attr( 'sidebar', 'home-' . $x ); ?>>

			<?php dynamic_sidebar( 'home-' . $x ); // Displays the subsidiary sidebar. ?>

		</aside><!-- #sidebar-subsidiary -->

	<?php endif; // End widgets check. ?>

<?php endfor; // end loop ?>
