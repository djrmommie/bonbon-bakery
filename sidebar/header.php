<aside <?php hybrid_attr( 'sidebar', 'header' ); ?>>

	<?php if ( is_active_sidebar( 'header' ) ) : // If the sidebar has widgets. ?>

		<?php dynamic_sidebar( 'header' ); // Displays the header sidebar. ?>

	<?php endif; // End widgets check. ?>

</aside><!-- #sidebar-primary -->