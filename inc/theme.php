<?php

# Register custom image sizes.
add_action( 'init', 'bonbon_bakery_register_image_sizes', 5 );

# Register custom menus.
add_action( 'init', 'bonbon_bakery_register_menus', 5 );

# Register custom layouts.
add_action( 'hybrid_register_layouts', 'bonbon_bakery_register_layouts' );

# Register sidebars.
add_action( 'widgets_init', 'bonbon_bakery_register_sidebars', 5 );

# Add custom scripts and styles
add_action( 'wp_enqueue_scripts', 'bonbon_bakery_enqueue_scripts', 5 );
add_action( 'wp_enqueue_scripts', 'bonbon_bakery_enqueue_styles',  5 );

# Show front page template only for show on front page
add_filter( 'frontpage_template', 'bonbon_bakery_front_page_template' );

# Filter the sidebar widgets.
add_filter( 'sidebars_widgets', 'bonbon_bakery_disable_sidebars' );
add_action( 'template_redirect', 'bonbon_bakery_theme_layout' );

# Register additional widgets.
add_action( 'widgets_init', 'bonbon_bakery_register_widgets' );

# Wrap logo in div
add_filter( 'get_custom_logo', 'bonbon_bakery_custom_logo' );

/**
 * Registers custom image sizes for the theme.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function bonbon_bakery_register_image_sizes() {

	// Sets the 'post-thumbnail' size.
	//set_post_thumbnail_size( 150, 150, true );
}

/**
 * Registers nav menu locations.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function bonbon_bakery_register_menus() {
	register_nav_menu( 'primary', esc_html_x( 'Primary', 'nav menu location', 'bonbon-bakery' ) );
	register_nav_menu( 'social', esc_html_x( 'Social Links', 'nav menu location', 'bonbon-bakery' ) );
}

/**
 * Registers layouts.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function bonbon_bakery_register_layouts() {

	hybrid_register_layout( '1c',   array( 'label' => esc_html__( '1 Column',                     'bonbon-bakery' ), 'image' => '%s/images/layouts/1c.png'   ) );
	hybrid_register_layout( '2c-l', array( 'label' => esc_html__( '2 Columns: Content / Sidebar', 'bonbon-bakery' ), 'image' => '%s/images/layouts/2c-l.png' ) );
	hybrid_register_layout( '2c-r', array( 'label' => esc_html__( '2 Columns: Sidebar / Content', 'bonbon-bakery' ), 'image' => '%s/images/layouts/2c-r.png' ) );
}

/**
 * Registers sidebars.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function bonbon_bakery_register_sidebars() {

	hybrid_register_sidebar(
		array(
			'id'          => 'primary',
			'name'        => esc_html_x( 'Primary', 'sidebar', 'bonbon-bakery' ),
			'description' => esc_html__( 'Main sidebar for 2 column design.', 'bonbon-bakery' )
		)
	);

	hybrid_register_sidebar(
		array(
			'id'          => 'subsidiary',
			'name'        => esc_html_x( 'Subsidiary', 'sidebar', 'bonbon-bakery' ),
			'description' => esc_html__( 'Add sidebar description.', 'bonbon-bakery' )
		)
	);

	hybrid_register_sidebar(
		array(
			'id'          => 'header',
			'name'        => esc_html_x( 'Header', 'sidebar', 'bonbon-bakery' ),
			'description' => esc_html__( 'Header widgets.', 'bonbon-bakery' )
		)
	);

    for ( $x = 1; $x <= 5; $x++ ) {

		hybrid_register_sidebar(
			array(
				'id'          => 'home-' . $x,
				'name'        => esc_html_x( 'Home Page ' . $x, 'sidebar', 'bonbon-bakery' ),
				'description' => esc_html__( 'Home page widgets.', 'bonbon-bakery' )
			)
		);

	}
}

/**
 * Load scripts for the front end.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function bonbon_bakery_enqueue_scripts() {
}

/**
 * Load stylesheets for the front end.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function bonbon_bakery_enqueue_styles() {

	// fonts
	wp_enqueue_style( 'bonbon-bakery-fonts', bonbon_bakery_fonts_url(), array(), null );

	// Load parent theme stylesheet if child theme is active.
	if ( is_child_theme() )
		wp_enqueue_style( 'hybrid-parent' );

	// Load active theme stylesheet.
	wp_enqueue_style( 'hybrid-style' );
}

/**
 *
 * google fonts url
 *
 * @credit http://themeshaper.com/2014/08/13/how-to-add-google-fonts-to-wordpress-themes/
 *
 * @return string
 */
function bonbon_bakery_fonts_url() {
	$fonts_url = '';

	/* Translators: If there are characters in your language that are not
	* supported by Lora, translate this to 'off'. Do not translate
	* into your own language.
	*/
	$neucha = _x( 'on', 'Neucha font: on or off', 'bonbon-bakery' );

	/* Translators: If there are characters in your language that are not
	* supported by Open Sans, translate this to 'off'. Do not translate
	* into your own language.
	*/
	$raleway = _x( 'on', 'Raleway font: on or off', 'bonbon-bakery' );

	if ( 'off' !== $neucha || 'off' !== $raleway ) {
		$font_families = array();

		if ( 'off' !== $neucha ) {
			$font_families[] = 'Neucha:400';
		}

		if ( 'off' !== $raleway ) {
			$font_families[] = 'Raleway:400,700';
		}

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

/**
 * Sets front-page.php to only be used on front page as home page
 * @since  1.0.0
 * @param  string    $template
 * @return string
 */
function bonbon_bakery_front_page_template( $template ) {
	return is_home() ? '' : $template;
}

/**
 * Disables sidebars if viewing a one-column page.
 *
 * @since 1.0.0
 * @access public
 * @param array $sidebars_widgets A multidimensional array of sidebars and widgets.
 * @return array $sidebars_widgets
 */
function bonbon_bakery_disable_sidebars( $sidebars_widgets ) {

	if ( current_theme_supports( 'theme-layouts' ) && !is_admin() ) {

		if ( 'layout-1c' == hybrid_get_theme_layout() ) {
			$sidebars_widgets['primary'] = false;
		}
	}

	return $sidebars_widgets;
}

/**
 * Function for deciding which pages should have a one-column layout.
 *
 * @since 1.0.0
 * @access public
 * @return void
 */
function bonbon_bakery_theme_layout() {

	if ( !is_active_sidebar( 'primary' ) ||
	    is_attachment() && wp_attachment_is_image() && 'default' == get_post_layout( get_queried_object_id() ) ||
		is_front_page() ) {
			add_filter( 'hybrid_get_theme_layout', 'bonbon_bakery_theme_layout_one_column' );
	}

}

/**
 * Filters 'get_theme_layout' by returning 'layout-1c'.
 *
 * @since 1.0.0
 * @access public
 * @param string $layout The layout of the current page.
 * @return string
 */
function bonbon_bakery_theme_layout_one_column( $layout ) {
	return '1c';
}

/**
 * Loads extra widget files and registers the widgets.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function bonbon_bakery_register_widgets() {

	/* Load and register the posts widget. */
	require_once( trailingslashit( get_template_directory() ) . 'inc/widget-hero.php' );
	register_widget( 'Bonbon_Baker_Hero_Widget' );
}

/**
 * Adds wrapper div to custom logo
 *
 * @since 1.0.0
 * @access public
 * @param $html *
 * @return string
 */
function bonbon_bakery_custom_logo( $html ) {
	return '<div class="site-logo">' . $html . '</div>';
}

