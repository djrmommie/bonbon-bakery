

<?php
/**
 * Handles the theme's theme customizer functionality.
 *
 * @package    BonbonBakery
 * @author     DJRthemes <info@djrthemes.com>
 * @copyright  Copyright (c) 2016, DJRthemes
 * @link
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Theme Customizer setup.
add_action( 'customize_register', 'bonbon_bakery_customize_register' );

# Add color scheme to body
add_action( 'body_class', 'bonbon_bakery_color_scheme_class' );

# Custom header, call late so child themes can override.
add_action( 'after_setup_theme', 'bonbon_bakery_custom_header_setup', 15 );

/**
 * Sets up the theme customizer sections, controls, and settings.
 *
 * @since  1.0.0
 * @access public
 * @param  object  $wp_customize
 * @return void
 */
function bonbon_bakery_customize_register( $wp_customize ) {

	// Add color scheme setting and control.
	$wp_customize->add_setting( 'color_scheme', array(
		'default'           => 'default',
		'sanitize_callback' => '',
		'transport'         => 'postMessage',
	) );
	$wp_customize->add_control( new Hybrid_Customize_Control_Palette( $wp_customize, 'color_scheme', array(
		'label'    => __( 'Color Scheme', 'bonbon-bakery' ),
		'choices'  => bonbon_bakery_get_colors(),
		'priority' => 1
	) ) );

	// Load JavaScript files.
	add_action( 'customize_preview_init', 'bonbon_bakery_enqueue_customizer_scripts' );

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector' => '.site-title a',
			'container_inclusive' => false,
			'render_callback' => 'bonbon_bakery_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector' => '.site-description',
			'container_inclusive' => false,
			'render_callback' => 'bonbon_bakery_customize_partial_blogdescription',
		) );
	}
}

/**
 * Loads theme customizer JavaScript.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function bonbon_bakery_enqueue_customizer_scripts() {

	// Use the .min script if SCRIPT_DEBUG is turned off.
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	wp_enqueue_script(
		'bonbon-bakery-customize',
		trailingslashit( get_template_directory_uri() ) . "js/customizer{$suffix}.js",
		array( 'jquery' ),
		null,
		true
	);
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @since Bonbon Bakery 1.0.0
 * @return void
 */
function bonbon_bakery_customize_partial_blogname() {
	bloginfo( 'name' );
}
/**
 * Render the site tagline for the selective refresh partial.
 *
 * @since Bonbon Bakery 1.0.0
 * @return void
 */
function bonbon_bakery_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

function bonbon_bakery_get_colors() {
	return array(
		'blueberry-waffle' => array( 'label' => 'Blueberry Waffle', 'colors' => array( '#66b4ca','#ead8b7') ),
		'raspberry-jam' => array( 'label' => 'Raspberry Jam', 'colors' => array('#e569a3', '#d4d4d4') )
	);
}

/**
 * Return color scheme
 *
 * @since Bonbon Bakery 1.0.0
 * @return string
 */
function bonbon_bakery_get_color_scheme() {
	return get_theme_mod( 'color_scheme', 'blueberry-waffle' );
}

/**
 * Add class to body for color scheme
 *
 * @since Bonbon Bakery 1.0.0
 * @param $classes
 * @return array
 */
function bonbon_bakery_color_scheme_class( $classes ) {
	$classes[] = 'color-scheme-' . esc_attr( bonbon_bakery_get_color_scheme() );
    return $classes;
}

/**
 * Adds support for the WordPress 'custom-header' theme feature and registers custom headers.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function bonbon_bakery_custom_header_setup() {

	$current_colors = bonbon_bakery_get_color_scheme();

	$colors = bonbon_bakery_get_colors();

	$primary_color = $colors[$current_colors]['colors'][0];

	add_theme_support(
		'custom-header',
		array(
			'default-image'          => '%s/images/default.png',
			'random-default'         => false,
			'width'                  => 1280,
			'height'                 => 400,
			'flex-width'             => true,
			'flex-height'            => true,
			'default-text-color'     => $primary_color,
			'header-text'            => true,
			'uploads'                => true,
			'wp-head-callback'       => 'bonbon_bakery_custom_header_wp_head'
		)
	);

	// Registers default headers for the theme.
	register_default_headers(
		array(
			'bonbon' => array(
				'url'           => '%s/images/default.png',
				'thumbnail_url' => '%s/images/' . $current_colors . '-bg-thumb.png',
				// Translators: Header image description.
				'description'   => __( 'Bonbon Bakery Header', 'bonbon-bakery' )
			)
		)
	);
}

/**1
 * Callback function for outputting the custom header CSS to `wp_head`.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function bonbon_bakery_custom_header_wp_head() {

	$current_colors = bonbon_bakery_get_color_scheme();

	$style = '';

	if ( get_header_image() && get_header_image() !== bonbon_bakery_get_default_header() ) { // header image is not default
		$style .= ".site-header { background: url(" . get_header_image() . ") no-repeat center center / cover; };\n";

	}

	if ( display_header_text() && $hex = get_header_textcolor() )

		$style .= "body.custom-header #site-title a { color: #{$hex}; }";

	echo "\n" . '<style type="text/css" id="custom-header-css">' . trim( $style ) . '</style>' . "\n";
}

/**
 * Returns a url for the default header image
 *
 * @return string
 */
function bonbon_bakery_get_default_header() {

	$default = get_theme_support( 'custom-header', 'default-image' );

	if ( is_string( $default ) )
		$default = sprintf( $default, get_template_directory_uri() );

	return esc_url_raw( set_url_scheme ( $default ) );
}
