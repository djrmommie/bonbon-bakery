<?php
/**
 * WooCommerce Filters
 */

# remove woo breacrumbs so we can use hybrid
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

# remove woo's default content wrappers
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

# remove sidebar cause we call it in the footer.php
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

add_action( 'woocommerce_before_main_content', 'bonbon_bakery_wc_before_main_content' );
add_action( 'woocommerce_after_main_content', 'bonbon_bakery_wc_after_main_content' );

/**
 * replace default content start with ours
 */
function bonbon_bakery_wc_before_main_content() { ?>
	<main <?php hybrid_attr( 'content' ); ?>>
<?php }

/**
 * replace default content end
 */
function bonbon_bakery_wc_after_main_content() { ?>
	</main><!-- #content -->
<?php }