<?php
/**
 * Box widget
 *
 * @package BonbonBakery
 * @subpackage Includes
 * @since 1.0.0
 */

/**
 * Hero Widget Class
 *
 * @since 1.0.0
 */
class Bonbon_Baker_Hero_Widget extends WP_Widget {

	var $defaults = array(
		'title' => '',
		'hero-image-id' => '',
		'hero-link' => ''
	);

	/**
	 * Set up the widget's unique name, ID, class, description, and other options.
	 *
	 * @since 1.0.0
	 */
	function __construct() {

		/* Set up the widget options. */
		$widget_options = array(
			'classname'   => 'hero-widget',
			'description' => esc_html__( 'Widget to add hero image', 'bonbon-bakery' )
		);

		/* Create the widget. */
		parent::__construct(
			'hero-widget',               // $this->id_base
			__( 'Bonbon Bakery Hero', 'bonbon-bakery' ), // $this->name
			$widget_options
		);

		add_action('admin_enqueue_scripts', array( $this, 'upload_scripts'));
	}

	/**
	 * add js for the media uploader
	 *
	 * @since 1.0.0
	 *
	 * @credit https://tommcfarlin.com/the-wordpress-media-uploader/
	 */
	public function upload_scripts() {
		wp_enqueue_media();

		wp_enqueue_script(
			'bonbon-media-uploader',
			get_template_directory_uri() . '/js/media-uploader.js'
		);
	}

	/**
	 * Outputs the widget based on the arguments input through the widget controls.
	 *
	 * @since 1.0.0
	 */
	function widget( $sidebar, $instance ) {
		extract( $sidebar );

		/* Merge the user-selected arguments with the defaults. */
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		$title = $instance['title'];
		$img = wp_get_attachment_image_src( intval( $instance['hero-image-id'] ), 'full' );

		if ( !empty( $img[0] ) ) {

			/* Output the theme's widget wrapper. */
			echo $before_widget; ?>

			<div class="hero-widget" style="background-image:url('<?php echo $img[0]; ?>');">
				<h2><?php if ( $title ) : echo wp_kses_post( $instance['title'] ); endif; ?></h2>
			</div>

			<?php

			/* Close the theme's widget wrapper. */
			echo $after_widget;
		}
	}

	/**
	 * Updates the widget control options for the particular instance of the widget.
	 *
	 * @since 1.0.0
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Set the instance to the new instance. */

		if ( wp_attachment_is_image( intval( $new_instance['hero-image-id'] ) ) ) {
			$instance['hero-image-id']    = intval( $new_instance['hero-image-id'] );
		} else {
			$instance['hero-image-id']    = '';
		}

		$instance['title']    = wp_kses_post( $new_instance['title'] );
		$instance['hero-link']    = esc_url_raw( $new_instance['hero-link'] );

		return $instance;
	}

	/**
	 * Displays the widget control options in the Widgets admin screen.
	 *
	 * @since 1.0.0
	 */
	function form( $instance ) {

		/* Merge the user-selected arguments with the defaults. */
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		?>

		<div>

			<p>
				<label>
					<?php esc_html_e( 'Image:', 'bonbon-bakery' ); ?>
				</label>
			</p>

			<div class="hero-widget-thumb">
				<?php

				if ( wp_attachment_is_image( intval( $instance['hero-image-id'] ) ) ) {
					echo wp_get_attachment_image( intval($instance['hero-image-id'] ), array( 200, null ), false, array( 'style' => 'max-width:100%' ) );
				}

				?>
			</div>

			<p>
				<a class="button hero-widget-choose-images"><span class="wp-media-buttons-icon"></span> <?php esc_html_e( 'Choose Image', 'bonbon-bakery' ); ?></a>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:', 'bonbon-bakery' ); ?>
					<input class="widefat hero-widget-title" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>"
					       type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
				</label>
			</p>

			<p>
				<label for="<?php echo $this->get_field_id( 'hero-link' ); ?>"><?php esc_html_e( 'Link To:', 'bonbon-bakery' ); ?>
					<input class="widefat hero-widget-link" id="<?php echo $this->get_field_id( 'hero-link' ); ?>" name="<?php echo $this->get_field_name( 'hero-link' ); ?>"
					       type="text" value="<?php echo esc_url( $instance['hero-link'] ); ?>" />
				</label>
			</p>

			<input type="hidden" class="hero-widget-id" name="<?php echo $this->get_field_name( 'hero-image-id' ); ?>" id="<?php echo $this->get_field_id( 'hero-image-id' ); ?>" value="<?php echo esc_attr( $instance['hero-image-id'] ); ?>" />
		</div>
		<div style="clear:both;">&nbsp;</div>
		<?php
	}
}