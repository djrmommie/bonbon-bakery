<?php

# Call late so child themes can override.
add_action( 'after_setup_theme', 'bonbon_bakery_custom_background_setup', 15 );

/**
 * Adds support for the WordPress 'custom-background' theme feature.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function bonbon_bakery_custom_background_setup() {

	add_theme_support(
		'custom-background',
		array(
			'default-color'    => 'ffffff',
			'default-image'    => get_template_directory_uri() . '/images/bg1.png',
			'wp-head-callback' => 'bonbon_bakery_custom_background_callback',
			'header_text' => '000000'
		)
	);
}

/**
 * This is a fix for when a user sets a custom background color with no custom background image.  What
 * happens is the theme's background image hides the user-selected background color.  If a user selects a
 * background image, we'll just use the WordPress custom background callback.  This also fixes WordPress
 * not correctly handling the theme's default background color.
 *
 * @link http://core.trac.wordpress.org/ticket/16919
 * @link http://core.trac.wordpress.org/ticket/21510
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function bonbon_bakery_custom_background_callback() {

	// Get the background image.
	$image = get_background_image();

	// Get the background color.
	$color = get_background_color();

	// return early if neither set
	if ( !$image && !$color ) {
		return;
	}

	$style = "";

	if ( $image ) {
		$image = " background-image: url('$image');";

		$repeat = get_theme_mod( 'background_repeat', get_theme_support( 'custom-background', 'default-repeat' ) );
		if ( ! in_array( $repeat, array( 'no-repeat', 'repeat-x', 'repeat-y', 'repeat' ) ) )
			$repeat = 'repeat';
		$repeat = " background-repeat: $repeat;";

		$position = get_theme_mod( 'background_position_x', get_theme_support( 'custom-background', 'default-position-x' ) );
		if ( ! in_array( $position, array( 'center', 'right', 'left' ) ) )
			$position = 'left';
		$position = " background-position: top $position;";

		$attachment = get_theme_mod( 'background_attachment', get_theme_support( 'custom-background', 'default-attachment' ) );
		if ( ! in_array( $attachment, array( 'fixed', 'scroll' ) ) )
			$attachment = 'scroll';
		$attachment = " background-attachment: $attachment;";

		$style .= $image . $repeat . $position . $attachment;

		$style = 'body.custom-background #container { ' . trim( $style ) . '}';
	}

	// Get the background color.
	$color = get_background_color();

	if ( $color ) {
		$style .= "body.custom-background { background-color: #{$color}; }";
	}

	if ( !$style )
		return;
?>
<style type="text/css" id="custom-background-css"><?php echo trim( $style ); ?></style>
<?php }