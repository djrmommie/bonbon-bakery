<!DOCTYPE html>
<html <?php language_attributes( 'html' ); ?>>

<head <?php hybrid_attr( 'head' ); ?>>
<?php wp_head(); // Hook required for scripts, styles, and other <head> items. ?>
</head>

<body <?php hybrid_attr( 'body' ); ?>>

	<div id="container">

		<div class="skip-link">
			<a href="#content" class="screen-reader-text"><?php esc_html_e( 'Skip to content', 'bonbon-bakery' ); ?></a>
		</div><!-- .skip-link -->

		<header <?php hybrid_attr( 'header' ); ?>>

			<div <?php hybrid_attr( 'branding' ); ?>>

				<?php if ( function_exists( 'the_custom_logo' ) ) {
					the_custom_logo();
				} ?>

				<?php if ( display_header_text() ) : // If user chooses to display header text. ?>
					<div class="header-text">
						<?php hybrid_site_title(); ?>
						<?php hybrid_site_description(); ?>
					</div>

				<?php endif; // End check for header text. ?>

			</div><!-- #branding -->

			<?php hybrid_get_sidebar('header'); // Loads the sidebar-home.php template. ?>

		</header><!-- #header -->

		<?php hybrid_get_menu( 'primary' ); // Loads the menu/primary.php template. ?>

		<?php if ( is_front_page() ) : ?>
			<?php hybrid_get_sidebar('home'); // Loads the sidebar-home.php template. ?>
		<?php endif; ?>

		<div id="main" class="site-main">

			<?php hybrid_get_menu( 'breadcrumbs' ); // Loads the menu/breadcrumbs.php template. ?>
